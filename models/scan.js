'use strict';
module.exports = (sequelize, DataTypes) => {
  const UNKNOWN = 'unknown';
  const INVALID = 'invalid';
  const VALID = 'valid';
  const Scan = sequelize.define('scan', {
    partNumber: DataTypes.STRING,
    manufacturer: DataTypes.STRING,
    description: DataTypes.STRING,
    detectionDepth: DataTypes.NUMERIC,
    frequency: DataTypes.NUMERIC,
    additionalInfo: DataTypes.JSON,
    measurements: DataTypes.JSON,
    outcome: {
      type: DataTypes.ENUM(UNKNOWN, INVALID, VALID),
      allowNull: false,
      defaultValue: UNKNOWN
    },
    pending: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    }
  }, {});
  Scan.associate = function(models) {
    // associations can be defined here
    models.scan.belongsTo(models.sequence);
    models.scan.belongsTo(models.material);
  };
  return Scan;
};
