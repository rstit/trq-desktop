'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('scans', 'additionalInfo', {
      type: Sequelize.JSON
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('scans', 'additionalInfo', {
      type: Sequelize.STRING
    });
  }
};
