'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    const tableDescription = await queryInterface.describeTable('sequences');
    if (tableDescription.pending) {
      return;
    }
    return queryInterface.addColumn('sequences', 'pending', {
      type: Sequelize.BOOLEAN
    });
  },
  down: async (queryInterface, Sequelize) => {
    const tableDescription = await queryInterface.describeTable('sequences');
    if (!tableDescription.pending) {
      return;
    }
    return queryInterface.removeColumn('sequences', 'pending', {
      type: Sequelize.BOOLEAN
    });
  }
};
