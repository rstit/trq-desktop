const ScanSetup = require('./ScanSetup');

module.exports = class Scanning extends ScanSetup {
  constructor(app) {
    super(app);
  }

  async setupScan({
    detectionDepth,
    frequency,
    partNumber,
    description
  }) {
    await this.selectFirstMaterialOnList();
    await this.submitScanStep();
    await this.fillDetectionDepth(detectionDepth);
    await this.submitScanStep();
    await this.clickEditFrequencyButton();
    await this.fillFrequency(frequency);
    await this.submitScanStep();
    await this.fillPartNumber(partNumber);
    await this.submitScanStep();
    await this.fillDescription(description);
    await this.submitScanStep();
  }

  async isWaitingForMeasurementsFramesFromDevice() {
    const result = await Promise.all([
      await this.app.client.isVisible('.scanning-process-idle'),
      await this.app.client.isVisible('.scanning-process-in-progress')
    ]);
    return result[0] && !result[1];
  }

  async isScansListEmpty() {
    const isNoScanMsgVisible = await this.app.client.isVisible('.scanning-process-no-scans');
    const scansList = await this.getCurrentScansList();
    return !scansList.length && isNoScanMsgVisible;
  }

  async getRecentScans() {
    const onElements = (items) => items.map(item => item.textContent);
    try {
      return await this.app.client.selectorExecute('trq-scans-list-view .scan-recent .scan-part-number', onElements);
    } catch(error) {
      return [];
    }
  }

  async getCurrentScansList() {
    const onElements = (items) => items.map(item => item.textContent);
    try {
      return await this.app.client.selectorExecute('.scanning-process-list-item', onElements);
    } catch(error) {
      return [];
    }
  }

  async getScansSummaryList() {
    const onElements = (items) => items.map(item => item.textContent);
    try {
      return await this.app.client.selectorExecute('trq-scan-summary [name="partNumber"]', onElements);
    } catch(error) {
      return [];
    }
  }

  async saveScans() {
    await this.app.client.click('trq-scan-summary-list button[type="submit"]');
    return await this.wait3s();
  }

  async deleteScanNthScanOnSummaryList(n) {
    await this.app.client.click(`trq-scan-summary-list trq-scan-summary:nth-child(${n}) button[name="delete"]`);
    return await this.wait3s();
  }

  async getDataFromFooter() {
    const results = await Promise.all([
      await this.app.client.getText('.scanning-process-scan header h2'),
      await this.app.client.selectorExecute('.scanning-process dd', (dd) => {
        const items = dd.map(item => item.textContent);
        return {
          description:    items[0],
          manufacturer:   items[1],
          permeability:   items[2],
          detectionDepth: items[3],
          resistance:     items[4],
          frequency:      items[5],
        }
      }),
    ]);
    return {
      partNumber: results[0],
      ...results[1]
    };
  }

  async clickScanEditButton() {
    await this.app.client.click('.scanning-process-scan button.btn-secondary');
  }

  async isEditScanOpened() {
    return await this.app.client.isVisible('trq-scan-edit-form');
  }

  async isScanSummaryOpened() {
    return await this.app.client.isVisible('trq-scan-summary-list');
  }

  async isEditScanFormPoppulated({
    partNumber,
    description
  }) {
    const results = await Promise.all([
      await this.app.client.getValue('.scan-form input[name="partNumber"]'),
      await this.app.client.getValue('.scan-form textarea[name="description"]'),
    ]);
    return partNumber === results[0] && description === results[1];
  }

  async clickAddManufacturerButton() {
    await this.app.client.click('button[name="addManufacturer"]');
  }

  async addManufacturer(manufacturer) {
    await this.app.client.setValue('input[name="manufacturer"]', manufacturer);
  }

  async getManufacturer() {
    return await this.app.client.getValue('input[name="manufacturer"]');
  }

  async clickAddAdditionalInfoButton() {
    await this.app.client.click('button[name="addAdditionalInfo"]');
  }

  async setAdditionalInfo(index, key, value) {
    await this.app.client.setValue(`input[name="additionalInfoLabel${index}"]`, key);
    await this.app.client.setValue(`input[name="additionalInfoValue${index}"]`, value);
  }

  async getAdditionalInfo(index) {
    return {
      key: await this.app.client.getValue(`input[name="additionalInfoLabel${index}"]`),
      value: await this.app.client.getValue(`input[name="additionalInfoValue${index}"]`),
    };
  }

  async deleteAdditionalInfo(index) {
    await this.app.client.click(`button[name="additionalInfoDelete${index}"]`);
    return await this.wait3s();
  }

  async isAdditionalInfoVisible(index) {
    const key = await this.app.client.isVisible(`input[name="additionalInfoLabel${index}"]`);
    const value = await this.app.client.isVisible(`input[name="additionalInfoValue${index}"]`);
    return key && value;
  }

  async cancelEditScan() {
    return await this.app.client.click('button[name="cancel"]');
  }

  async finishScanning() {
    return await this.app.client.click('button[name="finish"]');
  }

  async clickStartNewScan() {
    return await this.app.client.click('button[name="start-new-scan"]');
  }
}
