const { expect } = require('chai');
const testUtils = require('./utils');
const Connection = require('./Connection');

describe('Connection with device', () => {
  beforeEach(testUtils.beforeEach(Connection));
  afterEach(testUtils.afterEach());

  it('can connect to/disconnect from device automatically', async function () {
    this.timeout(60000);
    await this.page.waitUntilConnectedToDevice();
    expect(await this.page.isConnectedToDevice()).to.eq(true);

    // Simulate device shutdown are any other problem
    // which will result the device to be disconnected
    this.device.forceClientDisconnect();

    // We should be able to see a disconnection screen:
    await this.page.waitUntilDisconnectedFromDevice();
    expect(await this.page.isDisconnectedFromDevice()).to.eq(true);

    // Lets see if we can connect again:
    await this.page.waitUntilConnectedToDevice();
    expect(await this.page.isConnectedToDevice()).to.eq(true);
  });
});
