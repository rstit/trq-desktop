const Window = require('./Window');

module.exports = class Navigation extends Window {
  constructor(app) {
    super(app);
  }

  gotoScanHistory() {
    return this.app.client.click('nav a[href="#/scan-history"]');
  }

  gotoMaterials() {
    return this.app.client.click('nav a[href="#/materials"]');
  }

  gotoConnection() {
    return this.app.client.click('nav a[href="#/connection"]');
  }

  async gotoScanSetup() {
    return this.app.client.click('a[href="#/scan-setup/0"]');
  }

  goBack() {
    return this.app.client.click('.titlebar-backbutton');
  }

  isBackButtonVisible() {
    return this.app.client.isVisible('.titlebar-backbutton');
  }

  async isRecentScans() {
    const text = await this.app.client.getText('main .list-header');
    return text === 'Recent Scans';
  }

  async isScanHistory() {
    const text = await this.app.client.getText('main .list-header');
    return text === 'Scan History';
  }

  async isMaterials() {
    const text = await this.app.client.getText('main .list-header');
    return text === 'Materials';
  }

  async isConnection() {
    const text = await this.app.client.getText('main h1');
    return text === 'Scanner Connected!' || text === 'Scanner not found';
  }

  async isScanSetup() {
    return this.app.client.isVisible('trq-scan-setup');
  }

  async isScanningProcess() {
    return this.app.client.isVisible('trq-scanning-process');
  }

  toggleNavigation() {
    return this.app.client.click('nav .ms-Icon--GlobalNavButton');
  }

  isNavigationCollapsed() {
    return this.app.client.waitUntil(async () => {
      const size = await this.app.client.getElementSize('nav');
      return size.width === 48;
    }, 1500);
  }

  isNavigationExpanded() {
    return this.app.client.waitUntil(async () => {
      const size = await this.app.client.getElementSize('nav');
      return size.width === 192;
    }, 1500);
  }
}
