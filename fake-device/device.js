const net = require('net');
const logger = require('./logger');
const protocol = require('../src/backend/communication/protocol');
const adc = require('./generator.adc');
const acc = require('./generator.acc');
const robot = require("robotjs");

const {
  DEVICE_ID,
  DEVICE_NAME,
  DEVICE_FIRMWARE_VERSION,
  STATUS_WRONG_DATA_VALUES,
  BEEP_HEADER,
  FUNCTION_BEEP,
  READ_DATA_HEADER,
  WRITE_DATA_HEADER,
  DDS_CONFIG_STRUCT,
  HANDSHAKE,
  STATUS_OK,
  BATTERY_STATUS_STRUCT,
  ADC,
  ACC,
 } = require('../src/backend/constants');

const MEASUREMENTS_COUNT_IN_ONE_MESSAGE = 4;
const SAMPLING_MS = 20;
const MEASURE_INTERVAL_MS = SAMPLING_MS * MEASUREMENTS_COUNT_IN_ONE_MESSAGE;
const BATTERY_INTERVAL_MS = 2000;
const EXAMPLE_DATA_INVALID = 'invalid';
const EXAMPLE_DATA_VALID = 'valid';

const createState = () => ({
  connected: false,
  exampleDataType: EXAMPLE_DATA_INVALID,
  frequencyTuning: 0,
  delta: 0,
  triggerPressed: false,
});
const DEVICE_START_TIME = Date.now();

const state = createState();
const setState = newState => {
  Object.assign(state, newState);
  device.emit('state', state);
};

const getState = () => state;

const handleBeep = (buffer, socket) => {
  const { result } = protocol.read(buffer).beep();
  if (result.function === FUNCTION_BEEP) {
    const beep = {
      status: STATUS_OK,
    };
    const message = protocol.write().beepResponse(beep).result;
    socket.write(message);
  }
};

const handleReadData = (buffer, socket) => {
  const { result } = protocol.read(buffer).readData();
  switch(result.struct_id) {
    case HANDSHAKE:
      const handshake = {
        struct_id: HANDSHAKE,
        struct: {
          deviceId: DEVICE_ID,
          firmwareVersion: DEVICE_FIRMWARE_VERSION,
          deviceStatus: STATUS_OK,
          deviceName: DEVICE_NAME,
        },
        status: STATUS_OK
      };
      const message = protocol.write().readDataResponse(handshake).result;
      socket.write(message);
      return;
  }
}

const handleWriteData = (buffer, socket) => {
  const { result } = protocol.read(buffer).writeData();
  switch(result.struct_id) {
    case DDS_CONFIG_STRUCT:
      if (result.struct.frequencyTuning < 0) {
        const error = protocol.write().writeDataResponse({
          status: STATUS_WRONG_DATA_VALUES,
        }).result;
        socket.write(error);
        return;
      }
      setState({
        frequencyTuning: result.struct.frequencyTuning,
      });
      return;
  }
}

const createMessage = (values, type) => {
    const measure = {
      timestamp: Date.now() - DEVICE_START_TIME,
      delta: 2,
      count: values.length,
      type,
      values
    };
    const { result } = protocol.write().measureData(measure);
    const output = Buffer.allocUnsafe(result.length);
    result.copy(output);
    return output;
}

const fillArrayWithSamples = (arr, valueFactory, progress, dataType) => {
  arr.push(valueFactory(progress, dataType));
}

const sampleData = (progress, accValues, adcValues) => {
  return new Promise((resolve) => {
    fillArrayWithSamples(accValues, acc, progress);
    fillArrayWithSamples(adcValues, adc, progress, state.exampleDataType);
    setTimeout(() => resolve(), SAMPLING_MS);
  });
}

const sendMeasureDataInIntervals = (socket) => {
  let progress = 0;
  setInterval(async () => {
    const accValues = [];
    const adcValues = [];
    if (!state.connected || !state.triggerPressed || socket.destroyed) {
      return;
    }
    for (let i = 0; i < MEASUREMENTS_COUNT_IN_ONE_MESSAGE; ++i) {
      await sampleData(progress, accValues, adcValues);
    }
    const accMessage = createMessage(accValues, ACC);
    const adcMessage = createMessage(adcValues, ADC);
    socket.write(accMessage);
    socket.write(adcMessage);
    progress += MEASUREMENTS_COUNT_IN_ONE_MESSAGE;
  }, MEASURE_INTERVAL_MS); };

const sendBatteryStatusInIntervals = (socket) => {
  let token = setInterval(() => {
    if (socket.destroyed || !state.connected) {
      clearInterval(token);
      return;
    }
    const batteryLevel = Math.round(Math.random() * 100);
    const batteryStatusMessage = protocol.write().readDataResponse({
      struct_id: BATTERY_STATUS_STRUCT,
      struct: {
        batteryLevel,
      },
    }).result;
    socket.write(batteryStatusMessage);
  }, BATTERY_INTERVAL_MS);
}

let client;
const device = net.createServer(socket => {
  client = socket;
  setState({
    connected: true
  })
  logger.decorateWriteWithLogger(socket);
  socket.on('data', buffer => {
    logger.logInput(buffer);
    switch(buffer[0]) {
      case BEEP_HEADER:
        handleBeep(buffer, socket);
        return;
      case READ_DATA_HEADER:
        handleReadData(buffer, socket);
        return;
      case WRITE_DATA_HEADER:
        handleWriteData(buffer, socket);
        return;
    }
  });
  socket.on('end', () => {
    socket.destroy();
    setState({
      connected: false
    });
  })
  sendBatteryStatusInIntervals(socket);
  sendMeasureDataInIntervals(socket);
});

device.pressTrigger = () => {
  setState({
    triggerPressed: true,
  });
}

device.releaseTrigger = () => {
  setState({
    triggerPressed: false,
  });
}

let movingDeviceIntervalToken;

device.stopMovingTheDeviceAroud = () => {
  clearInterval(movingDeviceIntervalToken);
}

device.startMovingTheDeviceAroud = () => {
  robot.setMouseDelay(0);
  clearInterval(movingDeviceIntervalToken);
  const height = 100;
  const width = 100;
  const TAU = Math.PI * 2;
  let x = 0;
  let y = 0;
  movingDeviceIntervalToken = setInterval(() => {
    x++;
    if (x > width) {
      x = 0;
    }
    y = height * Math.sin((TAU * x) / width) + height;
    robot.moveMouseSmooth(x, y);
    if (!state.connected) {
      clearInterval(movingDeviceIntervalToken);
    }
  });
}

device.forceClientDisconnect = () => {
  if (client) {
    client.destroy();
    setState({
      connected: false
    });
  }
}

device.sendAlert = () => {
  const error = protocol.write().writeDataResponse({
    status: STATUS_WRONG_DATA_VALUES,
  }).result;
  client.write(error);
}

device.getCurrentFrequency = () => {
  return new Promise((resolve) => {
    // Lets wait for any lags on the network
    // (that device can run on localhost, but doesn't now have to).
    setTimeout(() => {
      const state = getState();
      resolve(state.frequencyTuning);
    }, 250);
  });
}

device.setState = setState;
device.getState = getState;

device.onOutput = (callback) => {
  logger.registerOutputHandler(callback);
}

device.onInput = (callback) => {
  logger.registerInputHandler(callback);
}

device.setInvalidData = () => {
  setState({
    exampleDataType: EXAMPLE_DATA_INVALID
  });
}

device.setValidData = () => {
  setState({
    exampleDataType: EXAMPLE_DATA_VALID
  });
}

module.exports = device;
