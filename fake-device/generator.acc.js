/**
 * Generates fake accelerometer readings.
 */
const robot = require("robotjs");

let prevX = 0;
let prevY = 0;

const MAX_VALUE = 255;

const normalize = (x, y) => {
 const sum = x + y;
 return {
   x: sum ? x / sum : 0,
   y: sum ? y / sum : 0,
 }
}
function generateFakeAccelerometerData() {
  const mouse = robot.getMousePos();
  const { x, y } = normalize(mouse.x - prevX, mouse.y - prevY);

  const xAxis = Math.abs(Math.min(x * MAX_VALUE, MAX_VALUE));
  const yAxis = Math.abs(Math.min(y * MAX_VALUE, MAX_VALUE));
  const zAxis = 0;

  prevX = mouse.x;
  prevY = mouse.y;

  return [
    xAxis,
    yAxis,
    zAxis
  ]
}

module.exports = generateFakeAccelerometerData;
