const fs = require("fs");
const WavDecoder = require("wav-decoder");

const readFile = (filepath) => {
  return new Promise((resolve, reject) => {
    fs.readFile(filepath, (err, buffer) => {
      if (err) {
        return reject(err);
      }
      return resolve(buffer);
    });
  });
};

const decodeFile = async (file) => {
  const buffer = await readFile(file);
  const audioData = await WavDecoder.decode(buffer);
  return audioData.channelData[0];
}

const decodeAllFiles = async (name) => {
  return [
    await decodeFile(`${__dirname}/${name}-01.wav`),
    await decodeFile(`${__dirname}/${name}-02.wav`),
    await decodeFile(`${__dirname}/${name}-03.wav`)
  ];
}

module.exports = {
  fetchInvalidExampleData() {
    return decodeAllFiles('invalid');
  },

  fetchValidExampleData() {
    return decodeAllFiles('valid');
  }
}
