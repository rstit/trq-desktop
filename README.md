# Achilles client

## Technology

* Electron
* SQLite3 as database
* Sequelize as ORM

## Requirements

* npx - allows running scripts with different Node.js version
* Node 7 - required for sqlite3 module build
* python 2.7 - for building electron with sqlite on windows

## Installation

You can clone it and init submodules by:
```
$ git clone --recurse-submodules [git_url]
```


First install Node.js dependencies:

```
$ npx -p node@7 npm install
```

Next step is to create configuration file:

```
$ cp .env.example .env
```

You can edit content of .env file. In this file are stored all environment variables used by the application.

Run the Electron application:

```
$ npx -p node@7 npm start
```

Init Device lib (submodule) by:
```
$ git submodule init
$ git submodule update
```

To pull changes for submodule run
```
$ git submodule update --remote [path_to_lib]
```
so e.g.
```
$ git submodule update --remote ./src/device-lib
```

### Documentation

To generate documentation pleas run:

```
$ npm run generate:docs
```

Documentation will be available in the */docs* directory. Open */docs/index.html* in your browser.

### Development

The application needs to connect to a device in local network.
There is available a mocked server that acts like a real device.

Below command will start the device:
```
$ npm run device
```

This device can be run on any machine (even at localhost, where the application is running) - there is only one requirement: in needs to be in the same network.

This fake-device has a basic UI with allows you to perform a mocked scanning process and to force-disconnect connected application.

Fake-device can be also run without the UI in a headless mode:

```
$ npm run device --headless
```

### Angular Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `npm run e2e` to execute the end-to-end tests via [Spectron](https://github.com/electron/spectron).

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## Building electron application

```
$ npx -p node@7 npm run build
```
## Testing

All tests can be run by executing `npm test`.

### Unit tests

```
$ npm run test:unit
```

Unit tests in watch mode:


```
$ npm run test:unit:watch
```


## Potential problems

### Windows build
Error when building app on windows:
**error MSB8020: The build tools for v140 (Platform Toolset = 'v140') cannot be found.**

Install http://landinghub.visualstudio.com/visual-cpp-build-tools

### SQLite - missing module

Missing sqlite3 module in electron after successfull build. Try to build sqlite3 from source for electron:

```
$ npx -p node@7 npm install sqlite3 --runtime=electron --target=1.7.6 --dist-url=https://atom.io/download/electron"
```


## FAQ

> Where is the sqlite database located?

It's located in user system directory. For example on Windows 10 it will be: `C:\Users\User\AppData\Roaming\achilles-client\db.sqlite`. To locate the correct path for your system run:

```
require('electron').app.getPath('userData');
```

or see the value of `USER_DATA_PATH` in `./src/db.js`

> How to change database structure without breaking clients existing database?

* Modify a model schema you want to change (models/*.js)
* Create a migration script for your change in migrations/*.js

See Sequelize documentation for more information about [migration](http://docs.sequelizejs.com/manual/tutorial/migrations.html) and [Query Interface](http://docs.sequelizejs.com/manual/tutorial/migrations.html#query-interface) used in migrations scripts.

> How can I add a new message type to be send/read from the device?

Link to protocol documentation: https://docs.google.com/spreadsheets/d/1Pv-kIN-eHXCosGDMRI6FDK7-YVfVKPBo-i1cElKbb2M/edit#gid=807718591
Notice: The implementation should always reflect the latests changes, any differences in naming or in the structure are prohibitet.

First of all, a new frame or struct must be defined. From technincal point of view a freame and struct are the same thing, but for readability and consistency with protocol documentation we are keeping them separated:

* Frame - for example `ReadDataResponse`, is a carierer of a payload we call a struct.
* Struct - detailed data for example: battery status or wifi configuration. Never send to the device as-is. Should be wrapped in a frame. See protocol documentation for listing of all frames and possible structs.

To create a new frame pleas read first documentation for [binary-protocol](https://github.com/oleksiyk/binary-protocol) - library for manipulating binary data. It allows to create a protocol with custom messages.


Lets say we want to create a `msg` struct. We need to define two methods: write and read:

```javascript
protocol.define('msg', {
  write: function (value) {},
  read: function () {}
});
```
Lets assume that the `msg` struct will consist of a header(UInt8) and a message (Text).

```javascript
protocol.define('msg', {
  write: function ({
    header,
    message
  }) {
    this.UInt8(header);
    this.Payload(message);
  },
  read: function () {
    this.UInt8('header');
    this.Payload('message');
  }
});
```

Unit8 is a function already existing in the library, but there is no such thing as a Payload, we need to define such function ourselves. This function also is a struct and is defined the same way:

```javascript
protocol.define('Payload', {
  write: function (content) {
    this.UInt32(content);
  },
  read: function () {
    this.UInt32('content');
  }
});
```

Therefore we can combine structs to create more complicated ones. See [binary-protocol](https://github.com/oleksiyk/binary-protocol) for more information.
