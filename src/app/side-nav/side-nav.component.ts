import { Component } from '@angular/core';
import { ConnectionService } from '../connection/connection.service';

import * as pkg from '../../../package.json';

@Component({
  selector: 'trq-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent {
  open = false;
  version: string;

  constructor(private _connectionService: ConnectionService) {
    this.version = pkg['version'];
  }

  toggleOpen() {
    this.open = !this.open;
  }

  getCurrentState() {
    return this._connectionService.scannerData.state;
  }

  getScannerName() {
    return this._connectionService.scannerData.deviceName;
  }

  getScannerBatteryLevel() {
    return this._connectionService.scannerData.battery;
  }

  getScannerBatteryClassName() {
    return this._connectionService.batteryLevelClassNumber();
  }
}
