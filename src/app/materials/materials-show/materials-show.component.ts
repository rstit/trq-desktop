import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IMaterial } from '../../../communication/Materials.adapter';

@Component({
  selector: 'trq-materials-show',
  templateUrl: './materials-show.component.html',
  styleUrls: ['./materials-show.component.scss']
})
export class MaterialsShowComponent implements OnInit {
  query: string;
  materialsArray: IMaterial[];

  constructor(private _router: Router) { }

  ngOnInit() {
  }

  selectedMaterialIdChange(materialId) {
    this._router.navigate(['/edit-material', materialId]);
  }
}
