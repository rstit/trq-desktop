import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialsShowComponent } from './materials-show.component';

describe('MaterialsShowComponent', () => {
  let component: MaterialsShowComponent;
  let fixture: ComponentFixture<MaterialsShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialsShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialsShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
