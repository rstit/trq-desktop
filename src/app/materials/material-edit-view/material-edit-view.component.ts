import { Component, EventEmitter, Input, OnInit, OnChanges, Output } from '@angular/core';
import { IMaterial } from '../../../communication/Materials.adapter';
import { UNITS } from '../../shared/constants';

@Component({
  selector: 'trq-material-edit-view',
  templateUrl: './material-edit-view.component.html',
  styleUrls: ['./material-edit-view.component.scss']
})
export class MaterialEditViewComponent implements OnInit, OnChanges {
  readonly UNITS = UNITS;

  @Input() material: IMaterial;
  @Output() saveMaterial: EventEmitter<IMaterial> = new EventEmitter();

  frequencyMultiplier = 1;
  displayedFrequency: number;

  constructor() { }

  ngOnInit() {
    if (!this.material) {
      this.material = {
        name: null,
        permeability: null,
        resistance: null,
        frequency: null
      };
    }
  }
  ngOnChanges(changes) {
    if (changes.material && changes.material.currentValue) {
      this.displayedFrequencyChange(this.material.frequency);
    }
  }

  displayedFrequencyChange(newDisplayedFrequency) {
    this.displayedFrequency = newDisplayedFrequency;
    this.material.frequency = newDisplayedFrequency * this.frequencyMultiplier;
  }

  frequencyMultiplierChange(newFrequencyMultiplier) {
    this.material.frequency = this.material.frequency / this.frequencyMultiplier * newFrequencyMultiplier;
    this.frequencyMultiplier = newFrequencyMultiplier;
  }

  submit() {
    this.saveMaterial.emit(this.material);
  }
}
