import { IOption } from '../../shared/dropdown/dropdown.component';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'trq-frequency-dropdown',
  template: `
  <trq-dropdown
    [options]="options"
    [disabled]="disabled"
    [selectedOption]="selectedOption"
    (selectedOptionChange)="selectedOptionChange($event)"
  ></trq-dropdown>`,
  styleUrls: ['./frequency-dropdown.component.scss']
})
export class FrequencyDropdownComponent implements OnInit {
  @Input() frequencyMultiplier;
  @Output() frequencyMultiplierChange: EventEmitter<number> = new EventEmitter();
  @Input() disabled;

  options: IOption[] = [
    {
      displayedValue: 'Hz',
      savedValue: 1
    },
    {
      displayedValue: 'kHz',
      savedValue: 1000
    },
    {
      displayedValue: 'MHz',
      savedValue: 1000000
    }
  ];
  selectedOption: IOption;

  constructor() { }

  ngOnInit() {
    this.selectedOptionChange(this.options[0]);
  }

  selectedOptionChange(option: IOption) {
    this.selectedOption = option;
    this.frequencyMultiplierChange.emit(+this.selectedOption.savedValue);
  }

}
