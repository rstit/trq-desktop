import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrequencyDropdownComponent } from './frequency-dropdown.component';

describe('FrequencyDropdownComponent', () => {
  let component: FrequencyDropdownComponent;
  let fixture: ComponentFixture<FrequencyDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrequencyDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrequencyDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
