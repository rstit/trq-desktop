const { remote: { BrowserWindow } } = (<any>window).require('electron');
const ElectronTitlebarWindows = (<any>window).require('electron-titlebar-windows');

export interface ITitlebarOptions {
  darkMode?: boolean;
  fullscreen?: boolean;
  backgroundColor?: string;
  color?: string;
  draggable?: boolean;
}

export default class TitleBar {
  backButtonElement: HTMLButtonElement;
  titleElement: HTMLElement;
  contextElement: HTMLElement;
  titlebar;

  private static createBackButtonElement(context): HTMLButtonElement {
    const backButtonElement = document.createElement('button');
    const backIcon = document.createElement('i');

    backButtonElement.appendChild(backIcon);

    backIcon.classList.add('ms-Icon');
    backIcon.classList.add('ms-Icon--Back');

    backButtonElement.classList.add('btn-primary');
    backButtonElement.classList.add('titlebar-backbutton');

    backButtonElement.addEventListener('click', (event) => {
      event.preventDefault();
      event.stopPropagation();
    });

    backButtonElement.addEventListener('dblclick', (event) => {
      event.preventDefault();
      event.stopPropagation();
    });

    context.appendChild(backButtonElement);

    return backButtonElement;
  }

  private static createTitleElement(context): HTMLElement {
    const titleElement = document.createElement('span');
    titleElement.classList.add('titlebar-title');
    context.appendChild(titleElement);
    return titleElement;
  }

  constructor(options: ITitlebarOptions = {}) {
    const initialFocusedWindow = BrowserWindow.getFocusedWindow();
    const isMaximized = !!(initialFocusedWindow && initialFocusedWindow.isMaximized());
    const defaultOptions = <ITitlebarOptions>{
      darkMode: false,
      fullscreen: isMaximized,
      backgroundColor: '#ffffff',
      color: '#000000',
      draggable: true,
    };

    this.contextElement = document.getElementById('electron-titlebar');
    this.titlebar = new ElectronTitlebarWindows(Object.assign({}, defaultOptions, options));

    this.backButtonElement = TitleBar.createBackButtonElement(this.titlebar.titlebarElement);
    this.titleElement = TitleBar.createTitleElement(this.titlebar.titlebarElement);

    this.titlebar.appendTo(this.contextElement);
    this.bindEvents();
  }

  private bindEvents() {
    this.titlebar.on('minimize', () => {
      const focusedWindow = BrowserWindow.getFocusedWindow();
      if (focusedWindow) {
        focusedWindow.minimize();
      }
    });

    this.titlebar.on('maximize', () => {
      const focusedWindow = BrowserWindow.getFocusedWindow();
      if (focusedWindow) {
        focusedWindow.unmaximize();
      }
    });

    // Notice: this "fullscreen" event is probably not what you think it is.
    // It refers to the state where window is indeed "maximized",
    // and not in a true "fullscreen".
    this.titlebar.on('fullscreen', () => {
      const focusedWindow = BrowserWindow.getFocusedWindow();
      if (focusedWindow) {
        focusedWindow.maximize();
      }
    });

    this.titlebar.on('close', () => {
      const focusedWindow = BrowserWindow.getFocusedWindow();
      if (focusedWindow) {
        focusedWindow.close();
      }
    });
  }

  public setTitle(value: string) {
    this.titleElement.textContent = value;
  }

  public showBackButton() {
    this.backButtonElement.classList.add('titlebar-backbutton--visible');
  }

  public hideBackButton() {
    this.backButtonElement.classList.remove('titlebar-backbutton--visible');
  }

  public registerBackEventHandler(handler: Function) {
    this.backButtonElement.addEventListener('click', () => handler());
  }
}
