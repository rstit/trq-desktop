import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IScan } from '../../../../communication/ScanningProcess.adapter';
import { saveScansToCSVFile } from '../../../../communication/export';
import { ScanningProcessService } from '../../../scanning/scanning-process/scanning-process.service';
import scansAdapter from '../../../../communication/scanningProcess';

@Component({
  selector: 'trq-scans-recent',
  templateUrl: './scans-recent.component.html'
})
export class ScansRecentComponent implements OnInit {
  query: string;
  scansList: IScan[];

  offset = 0;
  limit = 20;
  hasNextPage = true;
  recentScansIds: number[];

  constructor(
    private _router: Router,
    private _scanningProcessService: ScanningProcessService
  ) {}

  ngOnInit() {
    this.recentScansIds = this._scanningProcessService.getRecentScansIds();
    this.getScans();
  }

  queryChange(newQuery) {
    this.getScans();
  }

  scanSelected(scan) {
    this._router.navigate(['scan-preview', scan.id]);
  }

  getScans({
    offset = this.offset,
    limit = this.limit,
    reload = true
  } = {}) {
    if (reload) {
      this.offset = 0;
    }

    const request = this.query ? scansAdapter.scanSearch({
      query: this.query,
      offset,
      limit
    }) : scansAdapter.scanFindAll({
      offset,
      limit
    });

    return request
      .then(result => {
        this.hasNextPage = !!result.length;
        if (reload) {
          this.scansList = <IScan[]>result;
        }
        else {
          this.scansList = this.scansList.concat(<IScan[]>result);
        }
      })
      .catch(error => console.warn('Error when getting scans: ', error));
  }

  scansListInfiniteScrollCallback(event) {
    return this.getScans({
      offset: event.offset,
      limit: event.limit,
      reload: false
    })
      .then(data => {
        if (typeof (event.callback) === 'function') { event.callback(); }
        else { throw new Error('Infinite scroll callback is not a function or is not defined'); }
      });
  }

  startNewScan() {
    this._router.navigate(['scan-setup', 0]);
  }
}
