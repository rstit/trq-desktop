import { Input, Output, EventEmitter  } from '@angular/core';
import { IScan } from '../../../../communication/ScanningProcess.adapter';

export default abstract class EditableScan {
  @Input() scan: IScan = {
    id: null,
    partNumber: null,
    manufacturer: null,
    description: null,
    detectionDepth: null,
    frequency: null,
    additionalInfo: {},
    material: null
  };

  @Output() updateScan: EventEmitter<IScan> = new EventEmitter();

  manufacturerVisible: boolean;
  additionalInfoArrays: [string, string][] = [];

  deleteManufacturer() {
    this.scan.manufacturer = null;
    this.manufacturerVisible = false;
  }

  addManufacturer() {
    this.manufacturerVisible = true;
  }

  addAdditionalInfo() {
    this.additionalInfoArrays.push(['', '']);
  }
  changeAdditionalInfo(newValue, additionalInfo: [string, string], index: number) {
    additionalInfo[index] = newValue;
  }
  deleteAdditionalInfoByIndex(index) {
    this.additionalInfoArrays.splice(index, 1);
  }
  isAdditionalInfoValid() {
    return !this.additionalInfoArrays.find(additionalInfo => !additionalInfo[0] || !additionalInfo[1]);
  }

  getAdditionalInfoObject() {
    const additionalInfoObject = {};
    this.additionalInfoArrays.forEach(array => {
      additionalInfoObject[array[0]] = array[1];
    });
    return additionalInfoObject;
  }
  onUpdateScan() {
    this.scan.additionalInfo = this.getAdditionalInfoObject();
    this.updateScan.emit(this.scan);
  }
}
