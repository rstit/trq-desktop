import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IScan } from '../../../../communication/ScanningProcess.adapter';
import scansAdapter from '../../../../communication/scanningProcess';
import { decode } from '../../../shared/helpers';

@Component({
  selector: 'trq-scan-edit',
  templateUrl: './scan-edit.component.html',
  styleUrls: ['./scan-edit.component.scss']
})
export class ScanEditComponent implements OnInit {
  scanId: number;
  isFullscreen = false;
  goToPath: string;
  scan: IScan = {
    id: null,
    partNumber: null,
    manufacturer: null,
    description: null,
    detectionDepth: null,
    frequency: null,
    additionalInfo: {},
    material: null
  };
  manufacturerVisible: boolean;
  additionalInfoArrays: [string, string][] = [];

  constructor(private route: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const mode = params['mode'] || '';
      const path = params['path'] ? params['path'] : '/scan-history';
      this.isFullscreen = mode.toLowerCase() === 'fullscreen';
      this.goToPath = path;
      this.scanId = params['id'];
      this.getScan();
    });
  }

  getScan() {
    scansAdapter.scanFindById(this.scanId)
      .then(result => {
        this.scan = result;
      })
      .catch(error => console.warn('Error while getting scan with id ${this.scanId} : ', error));
  }

  closeEdit() {
    history.back();
  }

  updateScan(scan: IScan) {
    scansAdapter.scanUpdateById(this.scanId, scan)
      .then(result => this._router.navigate([this.goToPath]))
      .catch(error => console.warn('Error while updating scan with id ${this.scanId} : ', error));
  }

  deleteScan() {
    scansAdapter.scanDestroyById(this.scanId)
      .then(result => this._router.navigate([this.goToPath]))
      .catch(error => console.warn('Error while deleting scan with id ${this.scanId} : ', error));
  }

}
