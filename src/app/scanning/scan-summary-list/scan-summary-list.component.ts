import { OnInit, Component, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { IScan } from '../../../communication/ScanningProcess.adapter';
import { ScanningProcessService } from '../../scanning/scanning-process/scanning-process.service';

@Component({
  selector: 'trq-scan-summary-list',
  templateUrl: './scan-summary-list.component.html',
  styleUrls: ['./scan-summary-list.component.scss']
})
export class ScanSummaryListComponent implements OnInit {
  scans: IScan[] = [];

  constructor(
    private _router: Router,
    private _scanningProcessService: ScanningProcessService
  ) {
  }

  async ngOnInit() {
    this.scans = await this._scanningProcessService.fetchScansInSequence();
    if (!this.scans.length) {
      history.back();
    }
  }

  async editScan(scan: IScan) {
    this._router.navigate(['/scan-edit', scan.id, 'mode', 'fullscreen', 'path', '/scan-summary']);
  }

  async deleteScan(scan: IScan) {
    const confirmation = confirm(`Are you sure you want to delete scan ${scan.partNumber}?`);
    if (confirmation) {
      this._scanningProcessService.removeScan(scan);
    }
  }

  async retryScan(scan: IScan) {
    this._router.navigate(['/scanning-process', scan.id]);
  }

  updateScan(scan: IScan) {
    this._scanningProcessService.updateScan(scan);
  }

  async submit() {
    await this._scanningProcessService.saveScans();
    this._router.navigate(['/scan-recent']);
  }
}
