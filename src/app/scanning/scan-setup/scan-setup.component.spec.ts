import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScanSetupComponent } from './scan-setup.component';

describe('ScanSetupComponent', () => {
  let component: ScanSetupComponent;
  let fixture: ComponentFixture<ScanSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScanSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScanSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
