import { Injectable, EventEmitter } from '@angular/core';
import { IMeasurement, IScan, IScanSetup, Outcome } from '../../../communication/ScanningProcess.adapter';
import scansAdapter from '../../../communication/scanningProcess';
import { IMaterial } from '../../../communication/Materials.adapter';

// TODO: No clue from where the attentuation should be fetched. Probably from a material service.
// Waiting for response from the client. Right now this value is hardcoded here and in the scan-chart.
const ATTENTUATION_THRESHOLD = 85;

@Injectable()
export class ScanningProcessService extends EventEmitter<IScan[]> {
  scans: IScan[] = [];
  setup: IScanSetup = <IScanSetup>{}; // a template used to generate instances of scans
  sequenceId: string;
  material: IMaterial;
  recentScansIds: number[] = [];

  constructor() {
    super();
  }

  static calculateAbsoluteMotionFromACC(acc) {
    return Math.max(
      Math.abs(acc[0]),
      Math.abs(acc[1]),
      Math.abs(acc[2])
    );
  }

  static calculateValueFromADC(adc) {
    return Math.abs(adc.reduce((previous, current) => previous - current, 0));
  }

  calculateMeasurement(acc: number[], adc: number[], progress = 0): IMeasurement {
    const delta = progress + ScanningProcessService.calculateAbsoluteMotionFromACC(acc);
    const value = ScanningProcessService.calculateValueFromADC(adc);
    return { value, delta };
  }

  setSingleScan(scan: IScan) {
    this.clearScans();
    this.addScanToList(scan);
  }

  addScanToList(scan: IScan) {
    this.scans.unshift(scan);
    this.emit(this.scans);
  }

  async createScan(scan: IScan) {
    try {
      this.calcOutcome(scan);
      const createdScan = await scansAdapter.scanCreateInSequenceForMaterial({
        scanDefinition: scan,
        sequenceId: this.sequenceId,
        materialId: this.material.id,
      });
      this.addScanToList(createdScan);
      return createdScan;
    } catch (error) {
      console.warn('Error while creating scan: ', error);
    }
  }

  async updateScan(scan: IScan) {
    if (!scan.id) {
      return;
    }
    this.calcOutcome(scan);
    const index = this.scans.findIndex(scanOnList => scanOnList.id === scan.id);
    this.scans.splice(index, 1, scan);
    this.emit(this.scans);
    return await scansAdapter.scanUpdateById(scan.id, scan);
  }

  async removeScan(scan: IScan) {
    await scansAdapter.scanDestroyById(scan.id);
    const index = this.scans.findIndex(scanOnList => scanOnList.id === scan.id);
    this.scans.splice(index, 1);
    this.emit(this.scans);
  }

  hasAnyScans(): boolean {
    return this.getScansCount() > 0;
  }

  getScansCount(): number {
    return this.scans.length;
  }

  getLatestScan(): IScan {
    return this.scans[0];
  }

  getRecentScansIds(): number[] {
    return this.recentScansIds;
  }

  setSequenceId(sequenceId: string) {
    this.sequenceId = sequenceId;
  }

  setMaterial(material: IMaterial) {
    this.material = material;
  }

  calcOutcome(scan: IScan) {
    const measurementsOverThreshold = scan.measurements.filter(measurement => measurement.value >= ATTENTUATION_THRESHOLD);
    const hasMeasurementsOverThreshold = measurementsOverThreshold.length > 0;
    scan.outcome = hasMeasurementsOverThreshold ? Outcome.Invalid : Outcome.Valid;
  }

  async saveScans() {
    this.recentScansIds = [];
    const jobs = this.scans.map(scan => ({
        ...scan,
        pending: false,
      }))
      .map(scan => scansAdapter.scanUpdateById(scan.id, scan));
      await scansAdapter.markSequenceAsNotPending(this.sequenceId);
    return await Promise.all(jobs);
  }

  private clearScans() {
    this.scans.splice(0, this.scans.length);
  }

  clear() {
    this.clearScans();
    this.sequenceId = null;
  }

  setSetup(setup: IScanSetup) {
    this.setup = setup;
  }

  getSetup() {
    return this.setup;
  }

  getSequenceId() {
    return this.sequenceId;
  }

  async fetchScansInSequence(pending = true) {
    if (!this.sequenceId) {
      return;
    }
    const scans = await scansAdapter.scanFindAllInSequence({
      sequenceId: this.sequenceId,
      pending
    });
    this.clearScans();
    this.scans.push(...scans);
    return this.scans;
  }
}
