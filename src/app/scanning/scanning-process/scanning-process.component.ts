/* flow:
- initialize a scan in component
- user presses trigger
- scan gathers data
- user releases trigger
- scan is saved on backend
- new scan initialized in component, repeat
*/
import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/switchMap';
import { IScan, IScanSetup, IMeasurement } from '../../../communication/ScanningProcess.adapter';
import materials from '../../../communication/materials';
import { IMaterial } from '../../../communication/Materials.adapter';
import { unpackScanSetup } from '../../shared/helpers';
import measureObservable from '../../../communication/measure.observable';
import scansAdapter from '../../../communication/scanningProcess';
import ScansAdapter from '../../../communication/ScanningProcess.adapter';
import { ScanningProcessService } from './scanning-process.service';
import { encode } from '../../shared/helpers';
const SCANNING_TIMEOUT_MS = 250;

@Component({
  selector: 'trq-scanning-process',
  templateUrl: './scanning-process.component.html',
  styleUrls: ['./scanning-process.component.scss']
})
export class ScanningProcessComponent implements OnInit, OnDestroy {
  private _measureSubscription: Subscription;
  singleScan: IScan;
  scanTemplate: IScan = {
    id: null,
    frequency: null,
    detectionDepth: null,
    description: null,
    partNumber: null,
  };
  material: IMaterial = {
    name: null,
    permeability: null,
    resistance: null,
    frequency: null,
    createdAt: null,
    updatedAt: null,
  };
  measureInProgress = false;
  cumulativeDelta = 0;
  timeout: number;
  private _measureTimeout: number;

  scans: IScan[] = [];
  isMeasuringInProgress = false;
  measurements: IMeasurement[] = [];

  sequenceId: string;
  setup: IScanSetup = <IScanSetup>{}; // a template used to generate instances of scans
  selectedScan: IScan = <IScan>{}; // a selected scan that is displayed at the bottom and can be edited

  constructor(
    private _ngZone: NgZone,
    private route: ActivatedRoute,
    private _router: Router,
    private _scanningProcessService: ScanningProcessService
  ) {
    this._measureSubscription = measureObservable.subscribe((value) => {
      this._ngZone.run(() => {
        const { acc, adc, inProgress } = value;
        this.measureInProgress = !!inProgress;
        console.log('in progress', this.measureInProgress)
        if (this.measureInProgress && adc && acc) {
          this.saveMeasure(acc, adc);
        } else {
          if (this.isSingleScanMode()) {
            this.updateSingleScan();
            this.finish();
          } else {
            this.createNewScanFromSetup();
          }
        }
      });
    });
    this.scans = this._scanningProcessService.scans;
  }

  static unpackScanSetupFromURLTransfer(setup: IScanSetup): IScanSetup {
    return unpackScanSetup(setup);
  }

  stopMeasure() {
    this.measureInProgress = false;
    this.cumulativeDelta = 0;
  }

  startMeasure() {
    if (this.measureInProgress) {
      return;
    }
    this.cumulativeDelta = 0;
    this.measureInProgress = true;
  }

  isSingleScanMode() {
    return !!this.singleScan;
  }

  async initializeSingleScanModeForScanId(id) {
    if (typeof id === 'undefined') {
      return;
    }
    const scan = await scansAdapter.scanFindById(id);
    if (scan) {
      this.singleScan = scan;
      const setup = ScansAdapter.createSetupFromScan(this.singleScan);
      this._scanningProcessService.setSetup(setup);
      this._scanningProcessService.setSequenceId(this.singleScan.sequenceId);
    }
  }

  async recoverSetupForScanSequence() {
    this.setup = this._scanningProcessService.getSetup();
    this.sequenceId = this._scanningProcessService.getSequenceId();
    if (!this.sequenceId) {
      return await this.createNewSequence();
    }
  }

  async fetchScansInSequence() {
    return this._scanningProcessService.fetchScansInSequence();
  }

  ngOnInit() {
    this.route.params.subscribe(async params => {
      if (typeof params['id'] !== 'undefined') {
        this.initializeSingleScanModeForScanId(params['id']);
      }
      await this.recoverSetupForScanSequence();
      await this.fetchScansInSequence();
      await this.fetchMaterialFromSetup();
      this.selectScanOnList(this.getLatestScan());
    });
  }

  ngOnDestroy() {
    this._measureSubscription.unsubscribe();
  }

  async createNewSequence() {
    this.sequenceId = await scansAdapter.createSequence();
    this._scanningProcessService.setSequenceId(this.sequenceId);
  }

  async fetchMaterialFromSetup() {
    if (!this.setup.materialId) {
      return;
    }
    this.material = await materials.findById(this.setup.materialId);
    this._scanningProcessService.setMaterial(this.material);
  }

  async updateSingleScan() {
    if (!this.singleScan) {
      return;
    }
    this.singleScan.measurements = this.measurements;
    this.clearMeasurements();
    return await this.updateScan(this.singleScan);
  }

  async createNewScanFromSetup() {
    const newScan = <IScan>{};
    Object.assign(newScan, this.setup);
    newScan.partNumber = this.getNextPartNumber();
    newScan.measurements = this.measurements;
    this.clearMeasurements();
    const savedScan = await this.createScan(newScan);
    this.selectScanOnList(savedScan);
  }

  clearMeasurements() {
    this.measurements = [];
  }

  removeScanFromList(scan) {
    this._scanningProcessService.removeScan(scan);
  }

  hasScansOnList() {
    return this._scanningProcessService.hasAnyScans();
  }

  getNextPartNumber(): string {
    const { partNumber } = this.setup;
    const index = this._scanningProcessService.getScansCount();
    if (index === 0) {
      return partNumber;
    }
    return `${partNumber} (${index})`;
  }

  getAdditionalInfoEntries() {
    return Object.entries(this.selectedScan.additionalInfo || {});
  }

  getCurrentScanIndex() {
    return this._scanningProcessService.getScansCount() - 1;
  }

  getLatestScan(): IScan {
    return this._scanningProcessService.getLatestScan() || this.setup || <IScan>{};
  }

  saveMeasure(acc, adc) {
    const measurement = this._scanningProcessService.calculateMeasurement(acc, adc, this.cumulativeDelta);
    this.measurements.push(measurement);
    this.cumulativeDelta = measurement.delta;
  }

  selectScanOnList(scan: IScan) {
    this.selectedScan = scan;
  }

  setMaterial(material: IMaterial) {
    this.material = material;
  }

  openEditScan() {
    const path = this.isSingleScanMode() ? `/scanning-process/${this.singleScan.id}` : '/scanning-process';
    this._router.navigate(['/scan-edit', this.selectedScan.id, 'mode', 'fullscreen', 'path', path]);
  }

  openScanSummary() {
    this._router.navigate(['/scan-summary']);
  }

  finish() {
    this.openScanSummary();
  }

  cancel() {
    this._scanningProcessService.clear();
    this._router.navigate(['/']);
  }

  updateScan(scan: IScan) {
    return this._scanningProcessService.updateScan(scan);
  }

  createScan(scan: IScan) {
    return this._scanningProcessService.createScan(scan);
  }

  async editScan(scan: IScan) {
    this._router.navigate(['/scan-edit', scan.id, 'mode', 'fullscreen', 'path', '/scanning-process']);
  }

  async retryScan(scan: IScan) {
    this._router.navigate(['/scanning-process', scan.id]);
  }
}
