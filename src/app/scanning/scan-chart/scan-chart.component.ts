import { Component, ElementRef, OnDestroy, OnInit, OnChanges, ViewChild, Input } from '@angular/core';
import * as Chart from 'chart.js';
import { IMeasurement } from '../../../communication/ScanningProcess.adapter';

// TODO: What will be the real threshold value?
const ATTENTUATION_THRESHOLD = 85;

const COLOR_LINE = '#0080b7';
const COLOR_BACKGROUND0 =  'rgba(255,255,255,255)';
const COLOR_BACKGROUND1 =  'rgba(255,255,255,0)';
const COLOR_BREAKING_POINT = '#ce123f';
const COLOR_THRESHOLD = '#D0021B';

const defaultOptions = {
  responsive: true,
  maintainAspectRatio: false,
  animation: {
    easing: 'linear'
  },
  elements: {
    point: {
      radius: 0,
      hitRadius: 0
    }
  },
  tooltips: {
    enabled: false
  },
  legend: {
    display: false
  },
  gridLines: {
    drawBorder: false,
  },
  layout: {
    padding: {
      bottom: 80,
      right: 0,
      top: 10,
    },
  },
  scales: {
    yAxes: [{
      ticks: {
        suggestedMin: 0,
        suggestedMax: 1000,
        // stepSize: 10,
        maxTicksLimit: 40,
        display: true,
      }
    }],
    xAxes: [{
      display: false,
      type: 'time',
      distribution: 'series',
      ticks: {
        display: false,
        source: 'auto'
      }
    }]
  }
};

const plugins = [
  {
    id: 'resizableGradients',
    afterLayout: function (chart) {
      const { offsetHeight } = chart.ctx.canvas;
      const scale = chart.scales['y-axis-0'];
      const thresholdPx = scale.getPixelForValue(ATTENTUATION_THRESHOLD);
      const borderGradient = chart.ctx.createLinearGradient(0, thresholdPx, 0, thresholdPx + 50);
      borderGradient.addColorStop(0, COLOR_BREAKING_POINT);
      borderGradient.addColorStop(1, COLOR_LINE);

      const backgroundGradient = chart.ctx.createLinearGradient(0, thresholdPx, 0, scale.height);
      backgroundGradient.addColorStop(0.25, COLOR_BACKGROUND0);
      backgroundGradient.addColorStop(1, COLOR_BACKGROUND1);
      Object.assign(chart.config.data.datasets[0], {
        backgroundColor: backgroundGradient,
        borderColor: borderGradient,
      });
    }
  },
  {
    id: 'thresholdLine',
    afterDatasetsDraw: function (chart) {
      const width = chart.canvas.offsetWidth;
      const scale = chart.scales['y-axis-0'];
      const thresholdPx = scale.getPixelForValue(ATTENTUATION_THRESHOLD);

      chart.ctx.strokeStyle = COLOR_THRESHOLD;
      chart.ctx.lineWidth = 2;
      chart.ctx.beginPath();
      chart.ctx.moveTo(0, thresholdPx);
      chart.ctx.lineTo(width, thresholdPx);
      chart.ctx.closePath();
      chart.ctx.stroke();
    }
  }
];

@Component({
  selector: 'trq-scan-chart',
  templateUrl: './scan-chart.component.html',
  styleUrls: ['./scan-chart.component.scss']
})
export class ScanChartComponent implements OnInit, OnDestroy, OnChanges {
  private _dataInterval;
  private _dataIntervalTime = 500;
  private _visibleDataPoints = 150;

  @Input() measurements: IMeasurement[];
  @Input() size: number;

  chart: Chart;
  data: Chart.ChartPoint[] = [];
  dataset: any;

  @ViewChild('chartElement') chartElement: ElementRef;

  constructor() { }

  ngOnChanges(changes) {
    this.update();
  }

  ngOnInit() {
    const { nativeElement } = this.chartElement;
    const context = nativeElement.getContext('2d');

    this.dataset = {
      data: this.data,
      borderWidth: 2
    };

    const data = {
      datasets: [this.dataset]
    };

    this.chart = new Chart(context, {
      type: 'line',
      data,
      options: defaultOptions,
      plugins,
    });
  }

  capPoints(points) {
    const diff = points.length - this._visibleDataPoints;
    if (diff > 0) {
      points.splice(0, diff);
    }
    return points;
  }

  update() {
    if (!this.chart) {
      return;
    }
    const points = this.measurements.map((measurement) => ({
      x: measurement.delta,
      y: measurement.value,
    }));
    this.data.splice(0, this.data.length, ...this.capPoints(points));
    this.chart.update();
  }

  addPoint(point: Chart.ChartPoint) {
    this.data.push(point);
  }

  ngOnDestroy() {
    this.chart.destroy();
  }
}
