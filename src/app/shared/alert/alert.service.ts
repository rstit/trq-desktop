import { Injectable, NgZone } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { scannerObservable, ScannerMessageType, ScannerAlertType,
         IScannerMessage, IScannerAlertData } from '../../../communication/scanner.observable';

@Injectable()
export class AlertService {
  private _scannerSubscription: Subscription;

  private _isAlertOpen: boolean;
  private _alertData: IScannerAlertData = {
    alertType: null,
    code: null,
    message: null
  };

  constructor(private _ngZone: NgZone) {
    this._scannerSubscription = scannerObservable.subscribe((value: IScannerMessage) => {
      this._ngZone.run(() => {
        if (value.messageType === ScannerMessageType.alert) {
          this._alertData = <IScannerAlertData>{ ...value.data };
          this.openAlert();
        }
      });
    });
  }

  openAlert() {
    this._isAlertOpen = true;
    setTimeout(() => {
      this.closeAlert();
    }, 5000);
  }
  closeAlert() {
    this._isAlertOpen = false;
  }
  isAlertOpen() {
    return this._isAlertOpen;
  }
  getAlertData() {
    return this._alertData;
  }
}
