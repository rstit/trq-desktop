import { Component, HostBinding, OnInit } from '@angular/core';
import { AlertService } from './alert.service';

@Component({
  selector: 'trq-alert',
  template: `
    <div [class]="getClass()">
      <span>{{ getAlertMessage() }}</span>
      <button class="ms-Icon ms-Icon--ChromeClose" (click)="closeAlert()"></button>
    </div>
  `,
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
  constructor(private _alertService: AlertService) { }

  ngOnInit() {
  }

  getClass() {
    return `alert ${ this._alertService.getAlertData().alertType } ${ this._alertService.isAlertOpen() ? '' : 'closed' }`;
  }

  getAlertMessage() {
    return this._alertService.getAlertData().message;
  }
  closeAlert() {
    this._alertService.closeAlert();
  }

}
