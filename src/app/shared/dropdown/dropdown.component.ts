import { Component, Input, Output, EventEmitter, HostListener, ElementRef } from '@angular/core';

export interface IOption {
  displayedValue: string | number;
  savedValue: string | number;
}

@Component({
  selector: 'trq-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
})
export class DropdownComponent {
  open = false;

  @Input() options: IOption[];
  @Input() disabled = false;
  @Input() selectedOption: IOption;
  @Output() selectedOptionChange: EventEmitter<IOption> = new EventEmitter();

  @HostListener('document:click', ['$event']) clickOutside(event) {
    if (!this._element.nativeElement.contains(event.target)) {
      this.open = false;
    }
  }

  constructor(private _element: ElementRef) {
  }

  toggleDropdown() {
    if (this.disabled) {
      this.open = false;
      return;
    }
    this.open = !this.open;
  }

  selectOption(option: IOption) {
    this.selectedOptionChange.emit(option);
    this.open = false;
  }

  isSelected(option: IOption) {
    return option && this.selectedOption.savedValue === option.savedValue;
  }
}
