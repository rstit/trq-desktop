import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, AbstractControl } from '@angular/forms';
import { minValidator } from '../validators/min-max.validator';


@Directive({
  selector: '[trqMin]',
  providers: [{ provide: NG_VALIDATORS, useExisting: MinDirective, multi: true }]
})

export class MinDirective {
  @Input('trqMin') trqMin: number;

  validate(control: AbstractControl): { [key: string]: any } {
    return !isNaN(this.trqMin) ? minValidator(this.trqMin)(control) : null;
  }
}
