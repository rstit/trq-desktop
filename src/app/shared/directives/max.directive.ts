import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, AbstractControl } from '@angular/forms';
import { maxValidator } from '../validators/min-max.validator';


@Directive({
  selector: '[trqMax]',
  providers: [{ provide: NG_VALIDATORS, useExisting: MaxDirective, multi: true }]
})

export class MaxDirective {
  @Input('trqMax') trqMax: number;

  validate(control: AbstractControl): { [key: string]: any } {
    return !isNaN(this.trqMax) ? maxValidator(this.trqMax)(control) : null;
  }
}
