export type UUID = string;

export const UNITS = {
  resistance: 'Ω/cm',
  permeability: 'H/m',
  detectionDepth: 'm',
  frequency: 'Hz'
};

export const SCAN_SETUP_STORAGE_KEY = 'scan-setup';
