import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ConnectionService } from '../connection.service';

@Component({
  selector: 'trq-scanner-connected',
  templateUrl: './scanner-connected.component.html',
  styleUrls: ['./scanner-connected.component.scss']
})
export class ScannerConnectedComponent implements OnInit {

  constructor(private _connectionService: ConnectionService, private _router: Router) { }

  ngOnInit() {
  }

  getCurrentState() {
    return this._connectionService.scannerData.state;
  }

  getScannerName() {
    return this._connectionService.scannerData.deviceName;
  }

  getScannerId() {
    return this._connectionService.scannerData.deviceId;
  }

  getScannerBatteryLevel() {
    return this._connectionService.scannerData.battery;
  }

  getScannerBatteryClassName() {
    return this._connectionService.batteryLevelClassNumber();
  }
}
