const WiFi = require('./WiFi');
const wifi = require('node-wifi');

module.exports = new WiFi({ wifi });
