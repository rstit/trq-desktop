const ERROR_NOT_INITIALIZED = 'Wifi module not initialized. Call init() method on application startup.';

class WiFi {
  constructor({ wifi }) {
    this.initialized = false;
    this.wifi = wifi;
  }

  async init() {
    this.initialized = true;
    return await this.wifi.init({
      iface: null,
    });
  }

  async isConnected() {
    if (!this.initialized) {
      throw new Error(ERROR_NOT_INITIALIZED);
    }
    const connections = await this.wifi.getCurrentConnections();
    return connections.length > 0;
  }

  async scan() {
    return await this.wifi.scan();
  }

  async getCurrentConnections() {
    return await this.wifi.getCurrentConnections();
  }
}

module.exports = WiFi;
