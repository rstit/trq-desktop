const protocol = require('./protocol');

protocol.define('handshake', {
  write: function ({
    deviceId,
    firmwareVersion,
    deviceStatus,
    deviceName,
  }) {
    this.UInt32BE(deviceId)
      .UInt32BE(firmwareVersion)
      .UInt32BE(deviceStatus)
      .Char32(deviceName);
  },
  read: function () {
    this.UInt32BE('deviceId')
        .UInt32BE('firmwareVersion')
        .UInt32BE('deviceStatus')
        .Char32('deviceName');
  }
});

protocol.define('batteryStatus', {
  write: function({
    batteryLevel,
  }) {
    this.UInt16BE(batteryLevel);
  },

  read: function() {
    this.UInt16BE('batteryLevel');
  }
});

protocol.define('batteryConfig', {
  write: function({
    fctc,
    rcomp0,
    tempCo,
    tempNom,
    tempLim,
    vEmpty,
    fullCapNom,
    lavgEmpty,
  }) {
    this.UInt16BE(fctc)
        .UInt16BE(rcomp0)
        .UInt16BE(tempCo)
        .UInt16BE(tempNom)
        .UInt16BE(tempLim)
        .UInt16BE(vEmpty)
        .UInt16BE(fullCapNom)
        .UInt16BE(lavgEmpty);
  },
  read: function() {
    this.UInt16BE('fctc')
        .UInt16BE('rcomp0')
        .UInt16BE('tempCo')
        .UInt16BE('tempNom')
        .UInt16BE('tempLim')
        .UInt16BE('vEmpty')
        .UInt16BE('fullCapNom')
        .UInt16BE('lavgEmpty');
  }
});

protocol.define('status', {
  write: function({
    flags,
  }) {
    this.UInt32BE(flags);
  },

  read: function() {
    this.UInt32BE('flags');
  }
});

protocol.define('ddsConfig', {
  write: function({
    frequencyTuning,
    sweepParameter1,
    sweepParameter2,
    risingDelta,
    fallingDelta,
    risingSweepRampRate,
    fallingSweepRampRate,
    phaseOffset,
  }) {
    this.UInt32BE(frequencyTuning)
        .UInt32BE(sweepParameter1)
        .UInt32BE(sweepParameter2)
        .UInt32BE(risingDelta)
        .UInt32BE(fallingDelta)
        .UInt32BE(risingSweepRampRate)
        .UInt32BE(fallingSweepRampRate)
        .UInt16BE(phaseOffset);
  },

  read: function() {
    this.UInt32BE('frequencyTuning')
        .UInt32BE('sweepParameter1')
        .UInt32BE('sweepParameter2')
        .UInt32BE('risingDelta')
        .UInt32BE('fallingDelta')
        .UInt32BE('risingSweepRampRate')
        .UInt32BE('fallingSweepRampRate')
        .UInt16BE('phaseOffset');
  }
});

protocol.define('attenuatorsConfig', {
  write: function({
    value0,
    value1,
    value2,
  }) {
    this.UInt8(value0)
        .UInt8(value1)
        .UInt8(value2);
  },

  read: function() {
    this.UInt8('value0')
        .UInt8('value1')
        .UInt8('value2');
  }
});

protocol.define('wifiConfig', {
  write: function({
    ssid,
    password,
    flags,
  }){
    this.Char32(ssid)
        .Char32(password)
        .UInt16BE(flags);
  },
  read: function(){
    this.Char32('ssid')
        .Char32('password')
        .UInt16BE('flags');
  }
});

protocol.define('Char32', {
  write: function (value) {
    const target = Buffer.allocUnsafe(32).fill(0);
    Buffer.from(value, 'utf8').copy(target);
    this.raw(target);
  },
  read: function() {
    this.loop('chars', this.UInt8, 32);
    return this.context.chars.map(char => {
      return String.fromCharCode(char);
    }).join('');
  }
});
