const SORT_ASC = 'ASC';

const modelToJSON = result => result.get({ plain: true });

class MaterialService {
  constructor({ model }) {
    this.model = model;
  }

  async fetchMaterialsList(offset = 0, limit = 100, order = SORT_ASC) {
    const results = await this.model.findAll({
      order: [
        'name', order
      ],
      offset,
      limit,
    });
    return results.map(modelToJSON);
  }
}
