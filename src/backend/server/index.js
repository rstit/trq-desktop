const ipc = require('electron').ipcMain;
const database = require('../database');
const wifi = require('../wifi');
const Server = require('./Server');

const server = new Server({
  wifi,
  database,
  ipc,
});

module.exports = server;
