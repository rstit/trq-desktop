const MaterialProvider = require('../materials/material.provider');
const SequenceProvider = require('../sequences/sequence.provider');
const ScanProvider = require('../scans/scan.provider');

const ScanningProcessService = require('../scanningProcess/scanningProcess.service');
const Exporter = require('../exporter/Exporter');
const DeviceScanner = require('../device/DeviceScanner');
// const DeviceClient = require('../communication/DeviceClient');
const DeviceClient = require('../../device-lib');
const faker = require('faker');
faker.seed(1);

class Server {
  static resultToJSON(model) {
    return model.get({ plain: true });
  }
  constructor({
    wifi,
    database,
    ipc,
  }) {
    this.wifi = wifi;
    this.database = database;
    this.ipc = ipc;
    this.deviceScanner = new DeviceScanner();
    this.deviceConnection = null;
    this.client = null;

    this.deviceClient = new DeviceClient({ debug: false });
    this.deviceClientConnected = false;

    this.deviceClient.on('connected', () => {
      console.log('### Device Connected ###');
      this.client.send('handshake', { deviceName: 'STM Scanner', deviceId: 0x123, firmwareVersion: 1, deviceStatus: 0x0 })
      this.deviceClientConnected = true;
    })
    this.deviceClient.on('frameReceived', (data) => {
      const dataToSend = data.map(d => {
        d.data.values = d.data.items.map(v => [v.x, v.y, v.z])
        return d.data
      })
      this.client.send('measure', ...dataToSend);
    })
    this.deviceClient.on('disconnected', () => {
      console.log('!!!! disconnected !!!!');
      // this.client.send('client-disconnect');
      // console.log('reconnecting');
      // this.deviceClient.initConnection();
    })
  }

  bindListeners() {
    this.addEventListenerOnIPC('materialFindAll', async ({ offset, limit, order }) => {
      const materials = await this.materials.findAll({ offset, limit, order });
      return materials.map(Server.resultToJSON);
    });

    this.addEventListenerOnIPC('materialSearch', async ({ query, offset, limit, order }) => {
      const materials = await this.materials.search({ query, offset, limit, order });
      return materials.map(Server.resultToJSON);
    });

    this.addEventListenerOnIPC('materialCreate', async (material) => {
      return Server.resultToJSON(await this.materials.create(material));
    });

    this.addEventListenerOnIPC('materialFindById', async (id) => {
      return Server.resultToJSON(await this.materials.findById(id));
    });

    this.addEventListenerOnIPC('materialUpdateById', async ({ id, material }) => {
      return Server.resultToJSON(await this.materials.updateById(id, material));
    });

    this.addEventListenerOnIPC('materialDestroyById', async (id) => {
      return Server.resultToJSON(await this.materials.destroyById(id));
    });

    this.addEventListenerOnIPC('createSequence', async () => {
      const sequence = await this.scanningProcess.createSequence();
      return sequence.id;
    });

    this.addEventListenerOnIPC('scanCreateInSequenceForMaterial', async ({ scanDefinition, sequenceId, materialId }) => {
      const materialModel = await this.materials.findById(materialId);
      const sequenceModel = await this.sequences.findById(sequenceId);
      const scanModel = await this.scanningProcess.scanCreateInSequenceForMaterial({
        scanDefinition,
        sequenceModel,
        materialModel
      });
      return Server.resultToJSON(scanModel);
    });

    this.addEventListenerOnIPC('scanFindRelated', async ({ scanId, offset, limit, order }) => {
      const scanModel = await this.scans.findById(scanId);
      const scans = await this.scanningProcess.scanFindRelated({
        scanModel,
        offset,
        limit,
        order
      });
      return scans.map(Server.resultToJSON);
    });

    this.addEventListenerOnIPC('scanFindAllInSequence', async ({ sequenceId, pending }) => {
      const scans = await this.scanningProcess.scanFindAllInSequence({ sequenceId, pending });
      return scans.map(Server.resultToJSON);
    });

    this.addEventListenerOnIPC('saveScansToCSVFile', async ({ query, offset, limit, order }) => {
      const outputPath = await this.exporter.showSaveDialog();
      if (!outputPath) {
        return;
      }
      const exportedScans = await this.exporter.exportAllScans({ query, offset, limit, order });
      try {
        await this.exporter.saveToCSV(exportedScans, outputPath);
      } catch (error) {
        // @TODO: add error handling in TRQDA-41
      }
    });

    this.addEventListenerOnIPC('scanFindById', async (id) => {
      return Server.resultToJSON(await this.scans.findById(id));
    });

    this.addEventListenerOnIPC('scanUpdateById', async ({ id, scan }) => {
      try {
        return Server.resultToJSON(await this.scans.updateById(id, scan));
      } catch (message) {
        this.client.emit('scanner-alert', {
          alertType: 'error',
          message: message,
        });
      }
    });

    this.addEventListenerOnIPC('scanDestroyById', async (id) => {
      return Server.resultToJSON(await this.scans.destroyById(id));
    });

    this.addEventListenerOnIPC('scanSearch', async ({ query, offset, limit, order, where }) => {
      const scans = await this.scans.search({ query, offset, limit, order, where });
      return scans.map(Server.resultToJSON);
    });

    this.addEventListenerOnIPC('scanFindAll', async ({ offset, limit, order, where }) => {
      const scans = await this.scans.findAll({ offset, limit, order, where });
      return scans.map(Server.resultToJSON);
    });

    this.addEventListenerOnIPC('wifiIsConnected', async () => {
      return await this.wifi.isConnected();
    });

    this.addEventListenerOnIPC('wifiGetCurrectdeviceConnections', async () => {
      return await this.wifi.getCurrentdeviceConnections();
    });

    this.addEventListenerOnIPC('wifiScan', async () => {
      return await this.wifi.scan();
    });

    this.addEventListenerOnIPC('markSequenceAsNotPending', async (sequenceId) => {
      return await this.sequences.updateById(sequenceId, {
        pending: false
      });
    });

    this.addEventListenerOnIPC('sendDDSConfig', async (config) => {
      if (!this.deviceConnection) {
        return;
      }
      return await this.deviceConnection.sendDDSConfig(config);
    });

    this.addEventListenerOnIPC('setFrequency', async (frequency) => {
      return await new Promise(resolve => {
        this.deviceClient.setFrequency(frequency, resolve);
      });
    })

    this.bridgeDeviceAndClientConnection();
  }

  installProxyEventBetweenDeviceAndClient(eventName) {
    if (!this.deviceConnection) {
      return;
    }
    this.deviceConnection.on(eventName, (...payload) => {
      // Client has disconnected, but device is sending messages
      if (!this.client) {
        return;
      }
      this.client.send(eventName, ...payload);
    });
  }

  uninstallProxyEventBetweenDeviceAndClient(eventName) {
    if (!this.deviceConnection) {
      return;
    }
    this.deviceConnection.removeAllListeners(eventName);
  }

  findAndConnectToCompatibleDevice() {
    // Clean up
    this.destroyDeviceConnection();
    // Lets try to find again a device.
    this.establishDeviceConnection();
  }

  destroyDeviceConnection() {
    if (this.deviceConnection) {
      this.deviceConnection.destroy();
      this.deviceConnection = null;
    }

    // We need to remove all events listeners from device when the client disconnects.
    // Without this, there will be a memory leak and duplicated events on the client.
    this.uninstallProxyEventBetweenDeviceAndClient('handshake');
    this.uninstallProxyEventBetweenDeviceAndClient('disconnected');
    this.uninstallProxyEventBetweenDeviceAndClient('battery');
    this.uninstallProxyEventBetweenDeviceAndClient('measure');
    this.uninstallProxyEventBetweenDeviceAndClient('scanner-alert');
  }

  destroyClientConnection() {
    this.client = null;
  }

  establishClientConnection(client) {
    this.client = client;
  }

  async establishDeviceConnection() {
    if (!this.deviceConnection) {
      // First, we need to scan the network for a compatible device
      this.deviceConnection = await this.deviceScanner.findScanningDevice();
    }

    if (!this.deviceConnection) {
      return this.establishDeviceConnection();
    }

    this.installProxyEventBetweenDeviceAndClient('handshake');
    this.installProxyEventBetweenDeviceAndClient('disconnected');
    this.installProxyEventBetweenDeviceAndClient('battery');
    this.installProxyEventBetweenDeviceAndClient('measure');
    this.installProxyEventBetweenDeviceAndClient('scanner-alert');

    this.deviceConnection.sendHandShake();

    this.deviceConnection.on('close', () => {
      if (!this.client) {
        return;
      }
      if (this.isClientConnected()) {
        this.client.send('disconnected');
      }
      this.findAndConnectToCompatibleDevice();
    });

    return this.deviceConnection;
  }

  isClientConnected() {
    return this.client && !this.client.destroyed;
  }

  bridgeDeviceAndClientConnection() {
    const onClientConnect = (event, arg) => {
      console.log('on client connect');
      console.log('connecting to device');
      if (!this.deviceClientConnected) {
        this.deviceClient.initConnection();
      }
      this.establishClientConnection(event.sender);
      this.establishDeviceConnection();
    };

    const onClientDisconnect = (event, arg) => {
      this.destroyClientConnection();
      this.destroyDeviceConnection();
      this.deviceClient.socket.destroy();
      this.deviceClientConnected = false;
    };

    this.on('client-connect', onClientConnect);
    this.on('client-disconnect', onClientDisconnect);
  }

  on(eventName, callback) {
    this.ipc.on(eventName, callback);
  }

  off(eventName, callback) {
    this.ipc.removeListener(eventName, callback);
  }

  addEventListenerOnIPC(eventName, callback) {
    this.on(eventName, async (event, arg) => {
      const result = await callback(arg);
      event.sender.send(eventName, result);
    });
  }

  async init() {
    this.materials = new MaterialProvider({
      model: this.database.getModel('material'),
    });
    this.sequences = new SequenceProvider({
      model: this.database.getModel('sequence'),
    });
    this.scans = new ScanProvider({
      model: this.database.getModel('scan'),
      materialModel: this.database.getModel('material')
    });
    this.scanningProcess = new ScanningProcessService({
      materialProvider: this.materials,
      sequenceProvider: this.sequences,
      scanProvider: this.scans,
    });

    this.exporter = new Exporter({
      scanProvider: this.scans,
      materialProvider: this.materials,
    });

    await this.database.sync();
    await this.database.migrate();
    await this.wifi.init();

    this.bindListeners();

    await this.createFakeMaterials();
    await this.createFakesScans();
    await this.cleanUpPendingSequences();
    return await this.cleanUpPendingScans();
  }

  async cleanUpPendingScans() {
    const scans = await this.scans.findAll({
      where: {
        pending: false,
      }
    });
    return Promise.all(scans.map(scan => scan.destroy()));
  }

  async cleanUpPendingSequences() {
    const sequences = await this.sequences.findAll(0, undefined, undefined, {
      pending: true,
    });
    return Promise.all(sequences.map(sequence => sequence.destroy()));
  }

  async createFakeMaterials() {
    const anyMaterials = await this.materials.findAll();
    if (anyMaterials.length) {
      return;
    }
    for (let i = 0; i < 100; ++i) {
      await this.materials.create({
        name: faker.lorem.word(),
        permeability: faker.random.number() / 10000,
        resistance: faker.random.number() / 10000,
        frequency: faker.random.number() / 10000,
      });
    }
  }

  async createFakesScans() {
    const anyScans = await this.scans.findAll();
    if (anyScans.length) {
      return;
    }
    const materials = await this.materials.findAll();
    const sequenceModel = await this.scanningProcess.createSequence();
    for (let i = 0; i < 100; ++i) {
      const index = Math.round(Math.random() * (materials.length - 1))
      const materialModel = materials[index];
      const scanDefinition = {
        partNumber: `TRQ-${faker.random.number()}`,
        manufacturer: 'Triteq',
        description: faker.lorem.paragraphs(Math.random()),
        detectionDepth: faker.random.number() / 10000,
        frequency: faker.random.number() / 10000,
        additionalInfo: {
          foo: "bar"
        },
      };
      const scan = await this.scanningProcess.scanCreateInSequenceForMaterial({
        scanDefinition,
        sequenceModel,
        materialModel
      });
    }
  }
}

module.exports = Server;
