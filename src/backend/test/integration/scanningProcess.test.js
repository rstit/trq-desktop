const tmp = require('tmp');
const Database = require('../../database/Database');

const MaterialProvider = require('../../materials/material.provider');
const SequenceProvider = require('../../sequences/sequence.provider');
const ScanProvider = require('../../scans/scan.provider');

const ScanningProcessService = require('../../scanningProcess/scanningProcess.service');

let database;
let scanningService;

const partNumber = 'TRQ-123'
const manufacturer = 'Triteq';
const description = 'A scan';
const detectionDepth = 2.2;
const frequency = 5.234;
const additionalInfo = {};

const database_location = tmp.tmpNameSync();

const setup = async () => {
  database = new Database(database_location);

  const materialProvider = new MaterialProvider({
    model: database.getModel('material'),
  });

  const sequenceProvider = new SequenceProvider({
    model: database.getModel('sequences'),
  });

  const scanProvider = new ScanProvider({
    model: database.getModel('scans'),
  });

  scanningService = new ScanningProcessService({
    scanProvider, sequenceProvider, materialProvider
  });

  await database.authenticate();

  return await database.sync();
}

const tearDown = async () => {
  await database.getModel('scan').drop();
  await database.getModel('material').drop();
  return await database.getModel('sequence').drop();
}

describe('scanning process', () => {
  beforeEach(setup);
  afterEach(tearDown);
  it('creates scan in a sequance', async () => {
    const material = await scanningService.materialProvider.create({ name: 'foo' });
    const sequence = await scanningService.createSequence();
    const scanDefinition = {
      partNumber,
      manufacturer,
      description,
      detectionDepth,
      frequency,
      additionalInfo,
    };
    const scan = await scanningService.scanCreateInSequenceForMaterial(scanDefinition, sequence, material);
    expect(scan.sequenceId).toEqual(sequence.id);
  });

  describe('realted scans', () => {
    let scan0Definition;
    let scan1Definition;
    let scan2Definition;
    let scan3Definition;
    let scan0;
    let scan1;
    let scan2;
    let scan3;
    beforeEach(async () => {
      const material = await scanningService.materialProvider.create({ name: 'foo' });
      const sequence0 = await scanningService.createSequence();
      const sequence1 = await scanningService.createSequence();
      scan0Definition = { partNumber: 'TRQ-0' };
      scan1Definition = { partNumber: 'TRQ-1' };
      scan2Definition = { partNumber: 'TRQ-2' };
      scan3Definition = { partNumber: 'TRQ-3' };
      scan0 = await scanningService.scanCreateInSequenceForMaterial(scan0Definition, sequence0, material);
      scan1 = await scanningService.scanCreateInSequenceForMaterial(scan1Definition, sequence0, material);
      scan2 = await scanningService.scanCreateInSequenceForMaterial(scan2Definition, sequence0, material);
      scan3 = await scanningService.scanCreateInSequenceForMaterial(scan3Definition, sequence1, material);
    });

    it('retrieves other scans for given scan sequence', async () => {
      const relatedScans = await scanningService.scanFindRelated(scan0);
      expect(relatedScans[0].get('partNumber')).toEqual(scan2Definition.partNumber);
      expect(relatedScans[1].get('partNumber')).toEqual(scan1Definition.partNumber);
      expect(relatedScans.length).toEqual(2);
    });

    it('should not retrive scans from other sequences', async () => {
      const relatedScans = await scanningService.scanFindRelated(scan0);
      relatedScans.forEach((scan) => {
        expect(scan.get('partNumber')).not.toEqual(scan3Definition.partNumber);
      });
    });
  });
});
