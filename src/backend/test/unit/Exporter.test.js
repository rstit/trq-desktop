const Exporter = require('../../exporter/Exporter');

describe('Exporter addCSVExtension', () => {
  it('adds .csv extension', () => {
    const filename = '/path/to/file';
    const result = Exporter.addCSVExtension(filename);
    expect(result).toEqual(`${filename}.csv`);
  })
  it('does not add .csv extension if its already there', () => {
    const filename = '/path/to/file.csv';
    const result = Exporter.addCSVExtension(filename);
    expect(result).toEqual(filename);
  });
  it('is not case sensitive', () => {
    const filename = '/path/to/file.CSV';
    const result = Exporter.addCSVExtension(filename);
    expect(result).toEqual(filename);
  });
});
