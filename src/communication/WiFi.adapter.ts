import { send } from '../lib/ipcAdapter';

interface INetwork {
  ssid: string;
  bssid: string;
  mac: string; // equals to bssid (for retrocompatibility)
  channel: number;
  frequency: number; // in MHz
  signal_level: number; // in dB
  security: number; //
  security_flags: number; // encryption protocols (format currently depending of the OS)
  mode: number; // network mode like Infra (format currently depending of the OS)
}

interface IConnection extends INetwork {
  iface: string; // network interface used for the connection, not available on macOS
}

/**
 * Handles wifi connections.
 */
export default class WiFi {
  /**
   * Returns a promise, which value is true when we are connected to a network, or false otherwise.
   */
  async isConnected(): Promise<boolean> {
    return send<boolean>('wifiIsConnected');
  }

  /**
   * Returns a promise, which values is an array of available networks.
   */
  async scan(): Promise<INetwork[]> {
    return send<INetwork[]>('wifiScan');
  }

  /**
   * Returns a promise, which values is an array of networks to which we are connected.
   */
  async getCurrentConnections(): Promise<IConnection[]> {
    return send<IConnection[]>('wifiGetCurrectConnections');
  }
}
