const ipcRenderer = (<any>window).require('electron').ipcRenderer;
import { Observable } from 'rxjs/Observable';
import { zip } from 'lodash';

const ADC = 0x01;
const ACC = 0x02;

const MOVEMENT_THRESHOLD = 20;
// const SCANNING_TIMEOUT_MS = 250;
const SCANNING_TIMEOUT_MS = 2000;

const hasMovementOnAccelerometer = (values) =>  values.filter(value => Math.abs(value) > MOVEMENT_THRESHOLD).length > 0;

const measureObservable = Observable.create((observer) => {
  let timeout;
  let accValues = [];
  let adcValues = [];

  const onTimeout = () => {
    observer.next({
      inProgress: false
    });
  };

  const resetTimeout = () => {
    clearTimeout(timeout);
    timeout = setTimeout(onTimeout, SCANNING_TIMEOUT_MS);
  };

  const readMeasurements = (message) => {
    switch (message.type) {
      case ACC:
        accValues = message.values;
        break;
      case ADC:
        adcValues = message.values;
        break;
    }
  };

  const resetMeasurements = () => {
    accValues = [];
    adcValues = [];
  };

  const emitMeasurements = () => {
    zip(accValues, adcValues)
      .filter(onlyThoseWhichHaveMovement)
      .forEach((values) => {
        observer.next({
          acc: values[0],
          adc: values[1],
          inProgress: true,
        });
      });
    resetMeasurements();
  };

  const onlyThoseWhichHaveMovement = (values) => hasMovementOnAccelerometer(values[0]);

  const hasBothTypesOfMeasurements = (): boolean => accValues.length !== 0 && adcValues.length !== 0;

  ipcRenderer.on('measure', (event, message) => {
    let messageData = message;
    if (Array.isArray(messageData)) {
      messageData = message[0];
    }
    console.log('onMeasure', messageData);
    // Wee need to wait for both types of messages to arrive (ACC and ADC),
    // so we can see if there is any movement on the accelerometer
    readMeasurements(messageData);
    if (hasBothTypesOfMeasurements()) {
      emitMeasurements();
    }
    resetTimeout();
  });
});

export default measureObservable;
