/**
 * Instance of [[Materials.adapter]].
 */
import Materials from './Materials.adapter';
export default new Materials();
