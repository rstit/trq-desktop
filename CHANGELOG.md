# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.3.0"></a>
# [0.3.0](https://bitbucket.org/sebastianrosikrst/trq-desktop/compare/v0.2.0...v0.3.0) (2018-05-28)


### Features

* Added chart to scan preview ([ee939b8](https://bitbucket.org/sebastianrosikrst/trq-desktop/commits/ee939b8))
* Added new e2e test for recent scans ([c163616](https://bitbucket.org/sebastianrosikrst/trq-desktop/commits/c163616))
* Added second mode to scans-show component - recent scans ([04fa36e](https://bitbucket.org/sebastianrosikrst/trq-desktop/commits/04fa36e))
* Workign recent-scans feature ([d9dd822](https://bitbucket.org/sebastianrosikrst/trq-desktop/commits/d9dd822))



<a name="0.2.0"></a>
# [0.2.0](https://bitbucket.org/sebastianrosikrst/trq-desktop/compare/v0.1.1...v0.2.0) (2018-05-10)


### Bug Fixes

* build was throwing error ([43a5a3d](https://bitbucket.org/sebastianrosikrst/trq-desktop/commits/43a5a3d))
* Fixed TRQDA-76 by providing a routing behavior for summary list and scan edditing ([6d4830a](https://bitbucket.org/sebastianrosikrst/trq-desktop/commits/6d4830a))
* repaired broken tests ([20a9342](https://bitbucket.org/sebastianrosikrst/trq-desktop/commits/20a9342))


### Features

* Added scan retry ([19b8208](https://bitbucket.org/sebastianrosikrst/trq-desktop/commits/19b8208))
* Don't display the scan-summary if there are no scans ([a642d83](https://bitbucket.org/sebastianrosikrst/trq-desktop/commits/a642d83))



<a name="0.1.1"></a>
## [0.1.1](https://bitbucket.org/sebastianrosikrst/trq-desktop/compare/v0.1.0...v0.1.1) (2018-05-09)


### Bug Fixes

* Changed how layout works with horizontal padding and scrolling ([557eca0](https://bitbucket.org/sebastianrosikrst/trq-desktop/commits/557eca0))



<a name="0.1.0"></a>
# 0.1.0 (2018-05-08)


### Bug Fixes

* Changed more values from signed to unsigned integers (protocol) ([a95451c](https://bitbucket.org/sebastianrosikrst/trq-desktop/commits/a95451c))
* Changed values from int16be to uint16be. Also added max caps for ADC values ([2323c95](https://bitbucket.org/sebastianrosikrst/trq-desktop/commits/2323c95))
* npm run build was throwing error ([63fdcb4](https://bitbucket.org/sebastianrosikrst/trq-desktop/commits/63fdcb4))


### Features

* Added methods for refres/edit (their implementaions will be added in another story) ([1c9da4e](https://bitbucket.org/sebastianrosikrst/trq-desktop/commits/1c9da4e))
* Added new template for scan-summary component ([b904293](https://bitbucket.org/sebastianrosikrst/trq-desktop/commits/b904293))
* Applying real scan data to template ([a0ebba6](https://bitbucket.org/sebastianrosikrst/trq-desktop/commits/a0ebba6))
* changed how ADC values are attenuated on the client ([bc7de13](https://bitbucket.org/sebastianrosikrst/trq-desktop/commits/bc7de13))
* Displaying version number of the application in the side nav ([bb64015](https://bitbucket.org/sebastianrosikrst/trq-desktop/commits/bb64015))
* Saving scans after clicking save on scan-summary ([f46a79e](https://bitbucket.org/sebastianrosikrst/trq-desktop/commits/f46a79e))
* Saving scans. Marking them as not pending after saving. On Scan history displaying only those scans, that are not marked as pending (were not saved). ([08542c4](https://bitbucket.org/sebastianrosikrst/trq-desktop/commits/08542c4))
* Scan deletion from scan summary with a confirmation dialog ([346fd65](https://bitbucket.org/sebastianrosikrst/trq-desktop/commits/346fd65))
